
# FLEX BOX ADVENTURE

## MEDIUM - HARD

### **NIVEL 1**
para llevar a Arthur a la manzana se uso **“justify-content: center;”**  para alinear horizontalmente en el centro a Arthut y asi llegar a la manzana.
### **NIVEL 2**
Para llevar a Arthur a la zanahoria usamos **“justify-content: flex-end;”** para alinear a Arthur en el extremo opuesto y asi llegar a la zanahoria.
### **NIVEL 3**
o	Para llevar a Marilyn y Arthur al queso y pan y asi poder recuperar su vida se uso **“justify-content: space-between;”** para alinear a Arthur al inicio y a Marilyn al final.
### **NIVEL 4**
Nuevamente para que recuperen su vida ahora se uso **“justify-content: space-around;”**  para dar un espacio igual entre Arthur y Marilyn y también la mitad de ese espacio se coloca antes y después del primer y último elemento y asi llegar al objetivo.
### **NIVEL 5**
Nuevamente para que ambos recuperen su vida y llegar a su objetivo se uso “justify-content: space-evenly;”  crea un espacio igual entre Arthur y Marilyn y también en los extremos del contenedor.
### **NIVEL 6**
Se  agrego un nuevo personaje y para llegar al objetivo se uso un **“align-items: center;”** para alinear verticalmente los personajes
### **NIVEL 7**
Para llevar a los personajes verticalmente al final y asi llegar al objetivo se uso **“align-items: flex-end;”** para que los personajes se alinearán en la parte inferior del contenedor.
### **NIVEL 8**
Para llegar a los objetivos primero se uso **“justify-content: center;”** y asi centralos horizontalmente y luego se uso **“align-items: center;”** para centrar verticalmente y llegar al objetivo.
### **NIVEL 9**
Primero se uso un **“justify-content: space-between;”** par alinear horizontalmente a los 3 personajes uno al inicio otro al medio y otro al final  y después **“align-items: flex-end;”** para bajar al final a los personajes verticalmente y llegar al objetivo.
### **NIVEL 10**
Para llegar al objetivo se uso **“flex-direction: column;”** para que los personajes se apilen verticalmente .
### **NIVEL 11**
Para llegar al objetivo ahora se dio uso al **“flex-direction: row-reverse;”** para quue los personajes se coloquen horizontalmente.
### **NIVEL 12**
Primero se uso el **“flex-direction: column;”** para que los personajes se apilen verticalmente y después **“justify-content: flex-end;”** para que los personaje  llegen al final y asi al objetivo
### **NIVEL 13**
Se uso **“flex-direction: row-reverse;”** para que se orde en el sentido inverso después **“justify-content: center;”** para centrar horizontalmente y **“align-items: center;”** centrar de forma vertical
### **NIVEL 14**
Se uso **“order: 2 ”** para que el personaje seleccionado cambie lugares con el personaje 2
### **NIVEL 15**
Se uso **“align-self: center;”** para que el personaje seleccionado se centre verticalmente
### **NIVEL 16**
Primero se hace uso del **“order: 1;”** par que el personaje cambie lugar con el que esta en la posición 1 luego **“align-self: flex-end;”** para que nuevamente el personaje selccionado baje hacia el final verticalmente.-
### **NIVEL 17**
Se usa primero **“flex-direction: column-reverse;”** para que se apila una columna verticalmente con distinto sentido después **“justify-content: flex-end;”** para que se alinea horizontalmente al final y finalmente el **“align-items: center”** para centralos verticalmente
### **NIVEL 18**
Primero se usa **“order: 2”** para intercambiar posiciones el personaje selccionado con el que esta en la posición 2 y luego **“align-self: center;”** para que el personaje selcciona se centre verticalmente
### **NIVEL 19**
Se usa **“flex-wrap: wrap;”** para que cuando no quede espacio en la fila esta baje y se acomode y asi las monedas se acomodan
### **NIVEL 20**
Se usa **“align-content: center;”** para que se alinie en el centro
### **NIVEL 21**
Se usa primero **”align-content: flex-end;”** para llevar las monedas al final y luego **“justify-content: center;”** para que este se centre horizontalmente

![Captura de pantalla 2024-03-19 012918](https://github.com/Alexzc45/preciodelasbebidas/assets/145525730/a91ff9da-4002-4ed4-b507-8d6949cc9880)


### **NIVEL 22**
Primero se usa **“flex-direction: column-reverse;”** para apilar una columna verticalmente y de sentido contrario y luego **“justify-content: space-around;”** dar un espacio horizontalmente entre cada personaje después **“align-items: flex-end;”** para llevarlos verticalmente al final

![Captura de pantalla 2024-03-19 013022](https://github.com/Alexzc45/preciodelasbebidas/assets/145525730/f01117ca-3f46-4e2b-8a75-ff49d0990dce)


### **NIVEL 23**
Primero un **“flex-direction: column-reverse;”** para la columna con sentido opuesto **“justify-content: center;”** para centrar horizontalmente **“flex-wrap: wrap-reverse;”** para que si no vea espacio esta se mueva pero de sentido contrario **“align-content: center;”** para alinear y centrar verticalmente

![Captura de pantalla 2024-03-19 013136](https://github.com/Alexzc45/preciodelasbebidas/assets/145525730/0f882a6a-58dd-494b-b65a-9b3ec1c1cb74)

### **NIVEL 24**
Se usa **“order: 3** para que cambie de posición el personaje selccionado en este caso el 1 con el 3 despues **“align-self: center;”** para que centre verticalmente al personaje seleccionado

![Captura de pantalla 2024-03-19 013215](https://github.com/Alexzc45/preciodelasbebidas/assets/145525730/7d83f711-fb62-430f-be15-07eb7d786275)



















