
# 🌟ㅤ**PRACTICA 4**ㅤ🌟
# 🚀ㅤ**GRID ATTACK**ㅤ🚀 
![Captura de pantalla 2024-03-26 210539](https://github.com/Alexzc45/imagenesgit/assets/145525730/1e40790b-e1b8-43b4-96d1-63eee4e3e168)    
# 🎮ㅤ**PLAY**ㅤ🎮 
## **NIVELES:**
### *EAZY* 🟢
### *MEDIUM* 🟡
### *HARD* 🔴
# **HARD**
![Captura de pantalla 2024-03-26 210628](https://github.com/Alexzc45/imagenesgit/assets/145525730/956e7b71-4a87-4a85-8c3c-cb9c9066ddda)
# **NIVELES QUE SE DEBEN COMPLETAR:** 
### **NIVELES: 10-18-20-29-30-40-45-50-60-66-70-78-80** 


# 🚀 **NIVEL 10** 🚀

#
 ![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/276fc878-e9b3-4204-8d3c-2f48c5f45149)
#

## 🚀 **NIVEL 18** 🚀
### *DEBES DE DERROTAR A AZEL* 
![Captura de pantalla 2024-03-26 203907](https://github.com/Alexzc45/imagenesgit/assets/145525730/f5c26760-45af-4d08-836c-4974e728c631)
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/3319c942-cfd1-42d5-b9ea-1e2729adbe34)
#

## 🚀 **NIVEL 20** 🚀
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/f8c8cb17-7125-4f8a-8b5c-fe2bec925071)
#

## 🚀 **NIVEL 29** 🚀
### *DEBES DE DERROTAR A NIGHTCALL*
![Captura de pantalla 2024-03-26 203923](https://github.com/Alexzc45/imagenesgit/assets/145525730/44639fdd-0120-4320-a04c-6f9a67987b2c)
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/6658ca35-64c0-4f94-a4d7-c8d80484d1b0)
#

## 🚀 **NIVEL 30** 🚀
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/d598a1ee-718e-4a8b-99af-c45ca6b79365)
#

## 🚀 **NIVEL 40** 🚀
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/de10caad-e1b4-4895-8ab9-38526845e635)
#

## 🚀 **NIVEL 45** 🚀
### *DEBES DE DERROTAR A EVILINS*
![Captura de pantalla 2024-03-26 203946](https://github.com/Alexzc45/imagenesgit/assets/145525730/e1bc994d-0125-420e-884f-047f1cbe8da1)
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/d783dcbe-0184-4005-ba04-19458504038c)
#

## 🚀 **NIVEL 50** 🚀
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/ff985754-8a3a-474c-ac1e-9b17d1ecf1f1)
#

## 🚀 **NIVEL 60** 🚀
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/46b8567f-fdc6-4e46-af8c-8252d616a48f)
#

## 🚀 **NIVEL 66** 🚀
### *DEBES DE DERROTAR A ZARAX*
![Captura de pantalla 2024-03-26 203957](https://github.com/Alexzc45/imagenesgit/assets/145525730/7184bf62-8c35-4449-961e-5fdaa5d243fa)

#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/93c9c20a-14e2-4ffa-80c6-1955f03a21e0)
#

## 🚀 **NIVEL 70** 🚀
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/5a0ea62e-f413-4f90-9704-8558c4eb7f18)
#

## 🚀 **NIVEL 78** 🚀
### *DEBES DE DERROTAR A VALCORIAN*
![Captura de pantalla 2024-03-26 204010](https://github.com/Alexzc45/imagenesgit/assets/145525730/e2f556c5-8fac-4d20-a6c9-4999dff44af8)
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/3504fd0b-517b-4a6f-9cc0-cb6e6e5b9c03)
#

## 🚀 **NIVEL 80** 🚀
#
![image](https://github.com/Alexzc45/imagenesgit/assets/145525730/9b24d955-45bd-44d3-8e0a-95ae173c9a4f)
#
# **COMPLETADO**
## ~~10-18-20-29-30-40-45-50-60-66-70-78-80~~

# **FIN DEL JUEGO** 🏁 