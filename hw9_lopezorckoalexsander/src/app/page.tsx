import Button from "@/componentes/button";
import Letrash1 from "@/componentes/Letrash1";
import Parrafo from "@/componentes/Parrafo";

export default function Home() {
  return (
    <main>
      <div>
        <Button nombre={"HOLA"} ></Button>
        <Button nombre={"ADIOS"} botondos></Button>
      </div>
      <div className="contenedor1">
        <div>
          <Letrash1 pokemon={"type: Psyduck"}></Letrash1>
        </div>
        <div>
          <img src="./pokemon.svg" alt="" />
        </div>
        <div>
          <Parrafo parrafo={"Water"}></Parrafo>
        </div>
      </div>
      <div className="contenedor2">
        <div className="cuadrito1">
          <div>
            <Letrash1 pokemon={"la noche mas linda"}></Letrash1>
          </div>
          <div>
            <Parrafo parrafo={"Adalberto Santiago"}></Parrafo>
          </div>
          <div>
            <iframe width="200" height="300" src="https://www.youtube.com/embed/Be5fiuMGewA?si=pK5rsXmTn4zJgy34" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
          </div>
        </div>
        <div className="cuadrito2">
          <div>
            <Letrash1 pokemon={"nunca es suficiente"}></Letrash1>
          </div>
          <div>
            <Parrafo parrafo={"angeles azule"}></Parrafo>
          </div>
          <div>
            <iframe width="200" height="300" src="https://www.youtube.com/embed/k76BgIb89-s?si=XJ2gewC_gYzhzhYf" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
          </div>
        </div>
        <div className="cuadrito3">
          <div>
            <Letrash1 pokemon={"tu falta de querer"}></Letrash1>
          </div>
          <div>
            <Parrafo parrafo={"mon lafarte"}></Parrafo>
          </div>
          <div>
            <iframe width="200" height="300" src="https://www.youtube.com/embed/5R1RGl4WQP8?si=H8v8VlQ6vrPKdg92" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </main>
  );
} 
