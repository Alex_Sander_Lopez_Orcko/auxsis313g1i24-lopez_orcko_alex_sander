interface ButtonProps {
    nombre: string;
    botondos?: boolean;
  }
  
  export default function Button(props: ButtonProps) {
    return (
      props.botondos ?
        <button className="boton segundo-boton">{props.nombre}</button>
        :
        <button className="boton primer-boton">{props.nombre}</button>
    );
  }